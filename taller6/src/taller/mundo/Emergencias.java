package taller.mundo;

import java.util.ArrayList;

public class Emergencias 
{
	private Habitante habitante;
	private LocalizacionGeografica localizacion;
	private ArrayList<Habitante> habitantes;
	
	public Emergencias(Habitante phabitante, LocalizacionGeografica pLoGeografica) 
	{
		// TODO Auto-generated constructor stub
		habitante=phabitante;
		localizacion=pLoGeografica;
		habitantes= new ArrayList<>();
		
	}
	
	//-------------------------------
	//metododos
	//------------------------------------------------
	
	public Habitante darHabitante()
	{
	return	habitante;
	}
	
	public LocalizacionGeografica darLocalizacionGeografica()
	{
      return localizacion;
	}
	public ArrayList<Habitante> darHabitantes()
	{
		return habitantes;
	}
	
	public ArrayList<Habitante> agregarAListaDeEmergencias(Habitante pHabitante)
	{
		ArrayList<Habitante> emergencia= new ArrayList<>();
		emergencia.add(pHabitante);
		return emergencia;
	}
	
	public Habitante buscarHabitantePorNumero(Habitante pHabitante)
	{
		for (int i = 0; i < habitantes.size(); i++)
		{
			Habitante n= habitantes.get(i);
			if(pHabitante.darNumeroTelefono()==n.darNumeroTelefono())
			{
				return n;
			}
		}
		return null;
	}
	
	public Habitante buscarHabitantePorNombreOApellido(Habitante pHabitante)
	{
		for (int i = 0; i < habitantes.size(); i++) 
		{
			Habitante n= habitantes.get(i);
			if (n.darNombre().equals(pHabitante.darNombre())
					||n.darApellido().equals(pHabitante.darApellido()))
			{
				return n;
			}
		}
		return null;
	}
	
	public Habitante buscarHabitantePorLocalizacion(Habitante pHabitante )
	{
//		
//		if (localizacion.darLatitud()==pLocalizacionGeografica.darLatitud()
//				&& localizacion.darLongitud()==pLocalizacionGeografica.darLongitud()) 
//		{
//			
//			return 
//		}
		for (int i = 0; i < habitantes.size(); i++) 
		{
			Habitante n= habitantes.get(i);
			if (n.darCoordenadas()==pHabitante.darCoordenadas())
			{
				return n;
			}
		}
		return null;
	}
	
}
