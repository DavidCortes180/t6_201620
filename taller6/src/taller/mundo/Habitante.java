package taller.mundo;

public class Habitante 
{
	//nombre ciudadano
	private String nombre;
	//apellido ciudadano
	private String apellido;
	//numero telefonico
	private int numeroTelefono;
	
	private LocalizacionGeografica coordenadas;
	
	
	public Habitante(String pNombre, String pApellido, int pNumeroTelefono,LocalizacionGeografica pCoordenadas) 
	{
		// TODO Auto-generated constructor stub
		nombre=pNombre;
		apellido= pApellido;
		numeroTelefono=pNumeroTelefono;
		coordenadas=pCoordenadas;
	}
	
	//----------------------------------
	//Métodos-----------------------------------
	//--------------------------------------------------------------
	
	public String darNombre()
	{
		return nombre;
	}
	
	public String darApellido()
	{
		return apellido;
	}
	
	public int darNumeroTelefono()
	{
		return numeroTelefono;
	}
	public LocalizacionGeografica darCoordenadas()
	{
		return coordenadas;
	}
	
}
