package taller.mundo;

public class LocalizacionGeografica
{
	
	private int latitud;
	
	private int longitud;
	
	public LocalizacionGeografica(int pLatitud, int pLongitud)
	{
		// TODO Auto-generated constructor stub
		latitud=pLatitud;
		longitud=pLongitud;
	}
	
	//---------------------------
	//metodos
	//--------------------------------------
	
	public int darLatitud()
	{
	return latitud;	
	}
	
	public int darLongitud()
	{
	return longitud;	
	}
}
