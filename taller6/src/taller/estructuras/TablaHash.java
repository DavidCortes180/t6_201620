package taller.estructuras;

import java.util.ArrayList;
import java.util.Hashtable;


public class TablaHash<K extends Comparable<K> ,V> {

	//TODO Una enumeracioón que representa los tipos de colisiones que se pueden manejar
	

	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	//TODO: Agruegue la estructura que va a soportar la tabla.
	private ArrayList tablaHash;
	private NodoHash[] tupleKeyValue;
	

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;

	//Constructores

	@SuppressWarnings("unchecked")
	public TablaHash()
	{
		//TODO: Inicialice la tabla con los valores que considere prudentes para una ejecución normal
		tupleKeyValue= new NodoHash [2000000];
		capacidad= tupleKeyValue.length;
		count=0;
		
		for (int i = 0; i < tupleKeyValue.length; i++) 
		{
			tupleKeyValue[i]= new  NodoHash<Integer,NodoHash<K,V>>(0,null);
			tupleKeyValue[i].setLlave(i);
		}
		factorCarga=count/capacidad*100;
		factorCargaMax=2000000/capacidad*100;
		
	}

	@SuppressWarnings("unchecked")
	public TablaHash(int capacidadTabla, float pFactorCargaMax) 
	{
		//TODO: Inicialice la tabla con los valores dados por parametro
		capacidad= capacidadTabla;
		tupleKeyValue= new NodoHash[capacidadTabla];
		count=0;
		
		for (int i = 0; i < tupleKeyValue.length; i++)
		{
			tupleKeyValue[i]= new NodoHash<Integer,NodoHash<K,V>>(0,null);
			tupleKeyValue[i].setLlave(i);
		}
		factorCarga=count/capacidad;
		factorCargaMax=pFactorCargaMax;
	}

	public void put(K llave, V valor){
		//TODO: Gaurde el objeto valor dado por parametro el cual tiene la llave,
		//tenga en cuenta que puede o no puede haber colisiones
		
	}

	public V get(K llave){
		//TODO: Busque y retorne el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones

		return null;
	}

	public V delete(K llave)
	{
		//TODO: borra el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		for (int i = 0; i < tupleKeyValue.length; i++) 
		{
            V n= (V) tablaHash.get(i);
           if(tupleKeyValue[i]==llave)
           {
        	   tablaHash.remove(i);
        	   return n;
           }
		}
		return null;
	}

	//Hash
	private int hash(K llave)
	{
		//TODO: Escriba una función de Hash, recuerde tener en cuenta la complejidad de ésta así como las colisiones.
		return 0;
	}
	
		//TODO: Permita que la tabla sea dinamica

}